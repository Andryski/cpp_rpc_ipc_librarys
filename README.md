# List C++ rpc\ipc libraries 


If you have a project that should be listed here, please [send us a merge request](https://gitlab.com/Andryski/cpp_rpc_ipc_librarys/merge_requests) to update this page.

I meens rpc - Remote Procedure Call

ipc - Procedure Call through ipc (inter-process communication)


## RPC\IPC Implementations

| Name | ipc or rpc | info | Windows | Linux | Mac OS | Android | LICENSE |
| -------- | -------- | -------- | -------- | -------- |-------- |-------- |-------- |
|[mojo](https://github.com/chromium/chromium/tree/master/mojo)|ipc|chromium, IDL|||||?|
|||||||||
|[zeromq](http://zeromq.org/)|rpc\ipc*|[The IPC transport is currently only implemented on operating systems that provide UNIX domain sockets.](http://api.zeromq.org/4-2:zmq-ipc)|||||LGPL|
|[gRPC](http://www.grpc.io/)|rpc|http, IDL|||||Apache v2|
|[RPC on protobuf](https://github.com/protocolbuffers/protobuf/blob/master/docs/third_party.md#rpc-implementations)|rpc|IDL||||||
|[Cap'n Proto](https://capnproto.org/cxxrpc.html)|rpc,ipc*|rpc-?, ipc - [Unix domain socket can be specified as unix](https://capnproto.org/cxxrpc.html#starting-a-server), IDL|||||MIT|
|[Apache thrift](https://thrift.apache.org/)|rpc|TCP\HTTP, IDL|||||Apache v2|
|[rcf](http://www.deltavsoft.com/)|||||||GPL v2 \ self|
|[intra2net]()|rpc\ipc*|tcp/ip or local socket(a.k.a "unix domain socket"), IDL|||||gpl 2.1|
|[D-Bus](https://www.freedesktop.org/wiki/Software/dbus/)|||?||||GPL v2 orAFL 2.1|
|[nanorpc](https://github.com/tdv/nanorpc)|ipc|c++17|||-can't compile||MIT|
|[anyrpc](https://github.com/sgieseking/anyrpc)|rpc|tcp\HTTP, JsonRpc\XmlRpc\MessagePackRpc|||||MIT|
|[ice](https://github.com/zeroc-ice/ice)|rpc||||||GPL|
|[nng](https://github.com/nanomsg/nng)|rpc|?|||||MIT|
|||||||||
|[boost.interprocess<br>.message_queue](https://www.boost.org/doc/libs/1_70_0/doc/html/interprocess/synchronization_mechanisms.html#interprocess.synchronization_mechanisms.message_queue)|-,-|ipc message queue only|||||boost|
|[QT](https://doc.qt.io/qt-5/qlocalsocket.html)|-,-|Pipe only|+|+|+|+|LGPL|
|||||||||
|[msgpack:](https://msgpack.org/)||||||||
|[rpclib](https://github.com/rpclib/rpclib)|rpc|tcp?|||||[MIT](https://github.com/rpclib/rpclib/blob/master/LICENSE.md#license-terms-for-humans)|
|||||||||

### The following page are also of interest:
* [cppreference libs](https://ru.cppreference.com/w/cpp/links/libs)
* [awesome-cpp ipc](https://github.com/fffaraz/awesome-cpp)

